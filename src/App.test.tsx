import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import ReactDOM from 'react-dom';
import App from './App';

const renderer = new ShallowRenderer();
renderer.render(<App />);
const result = renderer.getRenderOutput();

describe('App', () => {
  it('renders correctly', () => {
    expect(result.type).toBe('main');
  })
});
