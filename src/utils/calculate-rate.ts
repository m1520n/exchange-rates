export const calculateRate = (amount: number, rate: number): number =>
  Math.round(amount * rate * 100) / 100;
