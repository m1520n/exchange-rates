import { calculateRate } from './calculate-rate';

describe('calculateRate', () => {
  [
    { amount: 100, rate: 1.34432, expected: 134.43 },
    { amount: 234.34, rate: 20.9873, expected: 4918.16 },
    { amount: 0, rate: 2.87687, expected: 0 },
    { amount: 100, rate: 0, expected: 0 },
    { amount: -1212, rate: 24.21123, expected: -29344.01 },
  ].forEach(({ amount, rate, expected }) => {
    it('calculates correct rate', () => {
      expect(calculateRate(amount, rate)).toEqual(expected);
    });
  });
});
