import React from 'react';

import GlobalStyles from './styles/global';

import Home from './containers/home/home';
import { Footer } from './components/footer/footer';
import { Header } from './components/header/header';

const App: React.FC = () => {
  return (
    <main>
      <Header />
      <Home />
      <Footer />
      <GlobalStyles />
    </main>
  );
};

export default App;
