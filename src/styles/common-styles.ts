import styled from 'styled-components';
import { colors } from './variables';

export const H1 = styled.h1`
  font-size: 2.5rem;
`;

export const H2 = styled.h2`
  font-size: 2rem;
`;

export const P = styled.p`
  font-size: 1.5rem;
`;

export const Date = styled.span`
  font-size: 2rem;
  font-style: bold;
  color: ${colors.asphaltBlack};
  margin: 0 1rem;
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const Amount = styled.span`
  font-size: 2rem;
  font-style: bold;
  color: ${colors.green};
  margin: 0 1rem;
`;
