import {
  Rates,
  HistoricalRates
} from '../../services/exchangeRateService.types';

export interface RatesStore {
  rates: Rates | null;
  historicalRates: HistoricalRates | null;
  fetchingRates: boolean;
  fetchingHistoricalRates: boolean;
  selectedCurrency: string;
  currencies: string[];
  date: string | null;
  amount: number;
}
