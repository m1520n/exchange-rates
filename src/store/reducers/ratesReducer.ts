import { RatesStore } from './rootReducer.types';

import { RateActionTypes } from '../actions/action.types';

import {
  FETCH_RATES_ATTEMPT,
  FETCH_RATES_SUCCESS,
  FETCH_RATES_FAILURE,
  FETCH_HISTORICAL_RATES_ATTEMPT,
  FETCH_HISTORICAL_RATES_SUCCESS,
  FETCH_HISTORICAL_RATES_FAILURE,
  SET_AMOUNT,
  SET_CURRENCY,
} from '../constants';

const initialState: RatesStore = {
  rates: null,
  historicalRates: null,
  fetchingRates: false,
  fetchingHistoricalRates: false,
  selectedCurrency: 'USD',
  currencies: [],
  date: null,
  amount: 0
};

export const ratesReducer = (
  state = initialState,
  action: RateActionTypes
): RatesStore => {
  switch (action.type) {
    case FETCH_RATES_ATTEMPT:
      return {
        ...state,
        fetchingRates: true
      };
    case FETCH_RATES_SUCCESS:
      return {
        ...state,
        fetchingRates: false,
        rates: action.rates,
        date: action.date,
        currencies: Object.keys(action.rates),
      };
    case FETCH_RATES_FAILURE:
      return {
        ...state,
        fetchingRates: false,
        rates: null,
        date: null
      };
    case FETCH_HISTORICAL_RATES_ATTEMPT:
      return {
        ...state,
        fetchingHistoricalRates: true
      };
    case FETCH_HISTORICAL_RATES_SUCCESS:
      return {
        ...state,
        fetchingHistoricalRates: false,
        historicalRates: action.historicalRates
      };
    case FETCH_HISTORICAL_RATES_FAILURE:
      return {
        ...state,
        fetchingHistoricalRates: false,
        historicalRates: null
      };
    case SET_AMOUNT:
      return {
        ...state,
        amount: action.amount,
      };
    case SET_CURRENCY:
      return {
        ...state,
        selectedCurrency: action.currency,
      };
    default:
      return state;
  }
};
