import { Dispatch } from 'redux';

import {
  FETCH_RATES_ATTEMPT,
  FETCH_RATES_SUCCESS,
  FETCH_RATES_FAILURE,
  FETCH_HISTORICAL_RATES_ATTEMPT,
  FETCH_HISTORICAL_RATES_SUCCESS,
  FETCH_HISTORICAL_RATES_FAILURE,
  SET_AMOUNT,
  SET_CURRENCY,
} from '../constants';

import { RateActionTypes, Payload } from './action.types';

import {
  getRatesForCurrency,
  getRatesForCurrencyBetweenDates
} from '../../services/exchangeRateService';

export const fetchRates = (currency: string) => async (
  dispatch: Dispatch<RateActionTypes>
) => {
  dispatch({ type: FETCH_RATES_ATTEMPT });
  try {
    const { rates, date } = await getRatesForCurrency(currency);
    dispatch({ type: FETCH_RATES_SUCCESS, rates, date });
  } catch (error) {
    dispatch({ type: FETCH_RATES_FAILURE });
  }
};

export const fetchHistoricalRates = (payload: Payload) => async (
  dispatch: Dispatch<RateActionTypes>
) => {
  dispatch({ type: FETCH_HISTORICAL_RATES_ATTEMPT });
  try {
    const { rates } = await getRatesForCurrencyBetweenDates(payload);
    dispatch({ type: FETCH_HISTORICAL_RATES_SUCCESS, historicalRates: rates });
  } catch (error) {
    dispatch({ type: FETCH_HISTORICAL_RATES_FAILURE });
  }
};

export const setAmount = (amount: number) => async (
  dispatch: Dispatch<RateActionTypes>
) => {
  dispatch({ type: SET_AMOUNT, amount });
};

export const setCurrency = (currency: string) => async (
  dispatch: Dispatch<RateActionTypes>
) => {
  dispatch({ type: SET_CURRENCY, currency });
};
