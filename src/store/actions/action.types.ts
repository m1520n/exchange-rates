import * as constants from '../constants';
import {
  Rates,
  HistoricalRates
} from '../../services/exchangeRateService.types';

export interface Payload {
  currency: string;
  start: string;
  end: string;
}

export interface FetchRatesAttempt {
  type: constants.FETCH_RATES_ATTEMPT;
}

export interface FetchRatesSuccess {
  type: constants.FETCH_RATES_SUCCESS;
  rates: Rates;
  date: string;
}

export interface FetchRatesFailure {
  type: constants.FETCH_RATES_FAILURE;
}

export interface FetchHistoricalRatesAttempt {
  type: constants.FETCH_HISTORICAL_RATES_ATTEMPT;
}

export interface FetchHistoricalRatesSuccess {
  type: constants.FETCH_HISTORICAL_RATES_SUCCESS;
  historicalRates: HistoricalRates;
}

export interface FetchHistoricalRatesFailure {
  type: constants.FETCH_HISTORICAL_RATES_FAILURE;
}

export interface SetAmount {
  type: constants.SET_AMOUNT;
  amount: number;
}

export interface SetCurrency {
  type: constants.SET_CURRENCY;
  currency: string;
}

export type RateActionTypes =
  | FetchRatesAttempt
  | FetchRatesSuccess
  | FetchRatesFailure
  | FetchHistoricalRatesAttempt
  | FetchHistoricalRatesSuccess
  | FetchHistoricalRatesFailure
  | SetAmount
  | SetCurrency;

export type AppActions = RateActionTypes;
