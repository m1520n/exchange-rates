import { createStore, combineReducers, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import thunk, { ThunkMiddleware } from 'redux-thunk';
import { ratesReducer } from './reducers/ratesReducer';
import { AppActions } from './actions/action.types';

export const rootReducer = combineReducers({
  rates: ratesReducer
});

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer);

export type AppState = ReturnType<typeof rootReducer>;

export const store = createStore(
  persistedReducer,
  applyMiddleware(thunk as ThunkMiddleware<AppState, AppActions>)
);

export const persistor = persistStore(store)
