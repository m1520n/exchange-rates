import React from 'react';

import { FooterWrapper, FooterText } from './footer.style';

export const Footer: React.FC = () => (
  <FooterWrapper>
    <FooterText>Developed by Michał Winiarski</FooterText>
  </FooterWrapper>
);
