import styled from 'styled-components';

import { colors } from '../../styles/variables';
import { P } from '../../styles/common-styles';

export const FooterWrapper = styled.footer`
  display: flex;
  width: 100%;
  height: 10rem;
  background-color: ${colors.asphaltBlack};
  padding: 1rem;
  align-items: center;
  justify-content: flex-start;
`;

export const FooterText = styled(P)`
  color: ${colors.cloudWhite};
`;
