import React from 'react';
import renderer from 'react-test-renderer';
import { Footer } from './footer';

const component = renderer.create(<Footer />);
const tree = component.toJSON();

describe('Footer', () => {
  it('renders correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
