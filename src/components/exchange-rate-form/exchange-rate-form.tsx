import React, { ChangeEvent } from 'react';
import { ValueType } from 'react-select/lib/types';

// types
import { Props, OptionType } from './echange-rate-form.types';

import {
  Form,
  InputWrapper,
  Input,
  GBP,
  StyledSelect
} from './exchange-rate-form.style';

export const ExchangeRateForm: React.FC<Props> = ({
  amount,
  selectedCurrency,
  currencies,
  onSetAmount,
  onChangeCurrency,
}) => {
  const handleSetAmount = (e: ChangeEvent<HTMLInputElement>) => {
    if (e) {
      e.preventDefault();
    }

    onSetAmount(e.currentTarget.value);
  };

  const handleChangeCurrency = (selectedOption: ValueType<OptionType>) => {
    const value = (selectedOption as OptionType).value;

    onChangeCurrency(value);
  };

  const options = currencies.map(currency => ({
    value: currency,
    label: currency
  }));

  return (
    <Form>
      <InputWrapper>
        <Input
          type="number"
          onChange={handleSetAmount}
          value={amount}
          step="0.01"
        />
        <GBP>GBP</GBP>
      </InputWrapper>
      <StyledSelect
        onChange={handleChangeCurrency}
        value={{ value: selectedCurrency, label: selectedCurrency }}
        options={options}
      />
    </Form>
  );
};
