import React from 'react';
import renderer from 'react-test-renderer';

import { ExchangeRateForm } from './exchange-rate-form';
import { Input, StyledSelect } from './exchange-rate-form.style';

const currencies = ['EUR', 'USD', 'PLN'];

const onSetAmount = jest.fn();
const onChangeCurrency = jest.fn();

const component = renderer.create(
  <ExchangeRateForm
    currencies={currencies}
    onSetAmount={onSetAmount}
    onChangeCurrency={onChangeCurrency}
  />
);
const tree = component.toJSON();

describe('ExchangeRateForm', () => {
  it('renders correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('invokes onSetAmount when user inputs value', () => {
    const input = component.root.findByType(Input);
    input.props.onChange({ currentTarget: { value: '100' }, preventDefault() {} });
    expect(onSetAmount).toHaveBeenCalledWith('100');
  });

  it('invokes onChangeCurrency when user selects option', () => {
    const select = component.root.findByType(StyledSelect);
    select.props.onChange({ value: 'USD' });
    expect(onChangeCurrency).toHaveBeenCalledWith('USD');
  });
});
