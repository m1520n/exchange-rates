export interface Props {
  amount: number;
  selectedCurrency: string;
  currencies: string[];
  onSetAmount: (amount: string) => void;
  onChangeCurrency: (currency: string) => void;
}

export interface OptionType {
  value: string;
  label: string;
}
