import styled from 'styled-components';
import Select from 'react-select';

import { colors } from '../../styles/variables';

export const Form = styled.header`
  width: 20rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
`;

// add media-queries

export const InputWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  margin: 1rem 0;
`;

export const Input = styled.input`
  height: 3.6rem;
  width: 20rem;
  font-size: 1.4rem;
  border-radius: 0.4rem;
  border-width: 0.1rem;
  border-style: solid;
  border-color: ${colors.black08};
  padding: 0 0.8rem;
  box-sizing: border-box;
`;

export const GBP = styled.span`
  font-size: 2rem;
  font-style: bold;
  margin-left: 1rem;
`;

export const StyledSelect = styled(Select)`
  width: 20rem;
`;
