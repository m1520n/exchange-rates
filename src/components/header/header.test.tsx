import React from 'react';
import renderer from 'react-test-renderer';
import { Header } from './header';

const component = renderer.create(<Header />);
const tree = component.toJSON();

describe('Header', () => {
  it('renders correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
