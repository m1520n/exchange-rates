import styled from 'styled-components';

import { colors } from '../../styles/variables';
import { H1 } from '../../styles/common-styles';

export const HeaderWrapper = styled.header`
  display: flex;
  width: 100%;
  height: 6rem;
  background-color: ${colors.green};
  padding: 1rem;
  align-items: center;
  justify-content: flex-start;
`;

export const HeaderText = styled(H1)`
  color: ${colors.cloudWhite};
`;
