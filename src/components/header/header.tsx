import React from 'react';

import { HeaderWrapper, HeaderText } from './header.style';

export const Header: React.FC = () => (
  <HeaderWrapper>
    <HeaderText>GBP currency converter tool</HeaderText>
  </HeaderWrapper>
);
