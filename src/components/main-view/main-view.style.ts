import styled from 'styled-components';

export const MainWrapper = styled.section`
  display: flex;
  width: 100%;
  height: 85vh;
  padding: 1rem;
  align-items: center;
  justify-content: flex-start;
  flex-direction: column;
`;
