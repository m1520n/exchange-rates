import React from 'react';

import { MainWrapper } from './main-view.style';

export const MainView: React.FC = ({ children }) => (
  <MainWrapper>{children}</MainWrapper>
);
