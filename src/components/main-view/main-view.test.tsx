import React from 'react';
import renderer from 'react-test-renderer';
import { MainView } from './main-view';

const component = renderer.create(<MainView />);
const tree = component.toJSON();

describe('MainView', () => {
  it('renders correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
