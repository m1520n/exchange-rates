import config from '../config';
import {
  Currency,
  CurrencyResponse,
  CurrencyHistoryResponse,
  HistoryParams
} from './exchangeRateService.types';

const { exchangeRateApi } = config;

export const getRatesForCurrency = async (
  currency: Currency
): Promise<CurrencyResponse> => {
  try {
    const response = await fetch(
      `${exchangeRateApi.url}/latest?base=${currency}`
    );
    const body = await response.json();
    return body;
  } catch (error) {
    console.log('Error fetching currency!', error);
    throw new Error(error);
  }
};

export const getRatesForCurrencyBetweenDates = async ({
  start,
  end,
  currency
}: HistoryParams): Promise<CurrencyHistoryResponse> => {
  try {
    const response = await fetch(
      `${exchangeRateApi.url}/history?start_at=${start}&end_at=${end}&base=${currency}`
    );
    const body = await response.json();
    return body;
  } catch (error) {
    console.log('Error fetching currency rates history!', error);
    throw new Error(error);
  }
};
