export type Currency = string;

export interface Rates {
  [key: string]: number;
}

export interface HistoricalRates {
  [key: string]: {
    [key: string]: string;
  };
}

export interface CurrencyResponse {
  base: string;
  date: string;
  rates: Rates;
}

export interface CurrencyHistoryResponse {
  base: string;
  rates: HistoricalRates;
}

export interface HistoryParams {
  currency: string;
  start: string;
  end: string;
}
