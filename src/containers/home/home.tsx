import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';

import { ExchangeRateForm } from '../../components/exchange-rate-form/exchange-rate-form';
import { MainView } from '../../components/main-view/main-view';

import { P, Date, Row, Amount } from '../../styles/common-styles';

//types
import { AppState } from '../../store';
import { AppActions } from '../../store/actions/action.types';
import { HomeState, HomeProps } from './home.types';
import { Rates } from '../../services/exchangeRateService.types';

import { calculateRate } from '../../utils/calculate-rate';
import { fetchRates, setAmount, setCurrency } from '../../store/actions/ratesActions';

interface LinkStateProps {
  fetchingRates: boolean;
  rates: Rates | null;
  selectedCurrency: string;
  currencies: string[];
  date: string | null;
  amount: number;
}

interface LinkDispatchProps {
  fetchRates: (currency: string) => void;
  setAmount: (amount: number) => void;
  setCurrency: (currency: string) => void;
}

type Props = HomeProps & LinkStateProps & LinkDispatchProps;

export class Home extends Component<Props, HomeState> {
  componentDidMount = () => {
    this.props.fetchRates('GBP');
  };

  handleSetAmount = (amount: string) => {
    this.props.setAmount(parseInt(amount || '0', 10));
  };

  handleSetTargetCurrency = (currency: string) => {
    this.props.setCurrency(currency);
  };

  render() {
    const { currencies, date, rates, selectedCurrency, amount } = this.props;

    if (!rates || !currencies || !date) return <p>loading!</p>; // TODO: add spinner and error handling if request fails

    const total = calculateRate(amount, rates[selectedCurrency]);

    return (
      <MainView>
        <P>This tool lets you convert GBP to a chosen currency.</P>
        <P>
          Please enter the amount and choose the currency you want to convert it
          to.
        </P>
        <Row>
          <P>Exchange rates from </P>
          <Date>{date}</Date>
        </Row>
        <ExchangeRateForm
          amount={amount}
          selectedCurrency={selectedCurrency}
          currencies={currencies}
          onSetAmount={this.handleSetAmount}
          onChangeCurrency={this.handleSetTargetCurrency}
        />
        <Row>
          TOTAL<Amount>{total}</Amount>
          {selectedCurrency}
        </Row>
      </MainView>
    );
  }
}

const mapStateToProps = (
  state: AppState,
  onwProps: HomeProps
): LinkStateProps => ({
  fetchingRates: state.rates.fetchingRates,
  rates: state.rates.rates,
  selectedCurrency: state.rates.selectedCurrency,
  currencies: state.rates.currencies,
  date: state.rates.date,
  amount: state.rates.amount
});

const mapDispatchToProps = (
  dispatch: ThunkDispatch<any, any, AppActions>,
  ownProps: HomeProps
): LinkDispatchProps => ({
  fetchRates: bindActionCreators(fetchRates, dispatch),
  setAmount: bindActionCreators(setAmount, dispatch),
  setCurrency: bindActionCreators(setCurrency, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
