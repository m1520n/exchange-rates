import { fetchRates } from '../../store/actions/ratesActions';
import {
  CurrencyResponse,
  Rates
} from '../../services/exchangeRateService.types';

export interface HomeProps {
  fetchingRates?: boolean;
  rates?: Rates | null;
  selectedCurrency?: string;
  currencies?: string[];
  date?: string | null;
  amount?: number;
  fetchRates?: (currency: string) => void;
  setAmount?: (amount: number) => void;
  setCurrency?: (currency: string) => void;
}

export interface HomeState {
}
